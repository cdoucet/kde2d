
import numpy


def histc(x, binranges):

    indices = numpy.searchsorted(binranges, x, side='right')

    if len(x) == 2:
        indices = numpy.mod(indices+1, len(binranges)+1)

    return indices
