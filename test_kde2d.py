
import numpy
import pytest

from kde2d import dct1d
from kde2d import dct2d
from kde2d import idct1d
from kde2d import idct2d
from kde2d import K
from kde2d import ndhist
from kde2d import kde2d

def test_dct1d():

  x = numpy.array([ [1, 2], [3, 4], [5, 6] ])
  weight = numpy.array([ [7, 8], [9, 10], [11, 12] ])

  actual = dct1d(x, weight)
  expected = numpy.array([ [77, 112], [-36, -40], [-44, -48] ])

  assert numpy.allclose(actual, expected, rtol=1e-15)  


def test_dct2d():

  data = numpy.array([ [1, 2], [3, 4] ])

  actual = dct2d(data)

  expected = numpy.array([ [10, -2.82842712474619], [-5.65685424949238, 0] ])

  assert numpy.allclose(actual, expected)


def test_idct1d():

  x = numpy.array([ [1, 2], [3, 4], [5, 6], [7, 8] ])
  weight = numpy.array([ [9, 10], [11, 12], [13, 14], [15, 16] ])

  actual = idct1d(x, weight)
  expected = numpy.array([ [53, 70], [-14, -16], [-14, -16], [-16, -18] ])

  assert numpy.allclose(actual, expected, rtol=1e-15)  

def test_idct2d():

  data = numpy.array([ [10, -2.82842712474619], [-5.65685424949238, 0] ])

  actual = idct2d(data)
  expected = numpy.array([ [1, 2], [3, 4] ])

  assert numpy.allclose(actual, expected, rtol=1e-15)  

def test_K():

  s = 1
  
  actual = K(s)
  expected = -0.398942280401433

  assert pytest.approx(actual, expected, 1e-15)

def test_ndhist():

  data = numpy.array([[1, 1], [1, 2]])
  M = 2
  
  actual = ndhist(data, M)
  expected = numpy.array([[0, 0], [0, 0.5]])

  assert numpy.allclose(actual, expected, rtol=1e-15)  


def test_kde2d():

#  data = numpy.array([1.63587024814945, 0.559018891329504, 0.563637584407024,
#                      -0.338396621324276, 0.763647371585355, 1.12744234603729,
#                      0.141571167263054, 1.92813410297015, 0.670637909553334,
#                      -0.110111236999403, 1.03480244733793, 2.64747205743548, 
#                      4.01173541276048, 3.75777491137441, 5.46244739500166,
#                      4.90631490892524, 3.99683373192458, 3.58283056227552,
#                      1.95152213502355, 5.36315963313024],
#                     [0.370962258011913, 1.09659469334834, -0.39816310715508,
#                      -0.0854625512885032, 2.37328741115302, -0.473921019935258,
#                      0.946340858418248, 0.818161575866987, 1.58897088443884,
#                      0.526011997212057, 0.134026621818908, -1.54600257382621,
#                      0.433282396191631, 0.102954219987465, -0.570346099876642,
#                      0.493062156693751, -0.707513213507977, -1.34802503597085,
#                      -1.7543015309362, -0.363808954647374])

  data = numpy.loadtxt('data.txt')
  
  bandwidth, density, X, Y = kde2d(data)

  expected_bandwidth = numpy.loadtxt('kde2d_bandwidth.txt')
  expected_density = numpy.loadtxt('kde2d_density.txt')
  expected_X = numpy.loadtxt('kde2d_X.txt')
  expected_Y = numpy.loadtxt('kde2d_Y.txt')

  assert numpy.allclose(bandwidth, expected_bandwidth, 1e-15)
  assert numpy.allclose(density, expected_density, 1e-15)
  assert numpy.allclose(X, expected_X, 1e-15)
  assert numpy.allclose(Y, expected_Y, 1e-15)




