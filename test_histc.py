
import numpy
import numpy.testing

from histc import histc

def test_histc():

  x = numpy.array([1, 2])
  binranges = numpy.array([0, 0.5, 1])

  actual = histc(x, binranges)
  expected = numpy.array([3, 0])

  numpy.testing.assert_array_equal(actual, expected)

  # another test

  x = numpy.array([0.393560875384953, 0.269802866632061, 0.270333673650778,
                   0.166666666666667, 0.293319959395337, 0.335129389589527,
                   0.221827351012658, 0.427149534092566, 0.282630772124169,
                   0.192902548167403, 0.32448267468176, 0.509820027379499,
                   0.666609091348513, 0.637422476358082, 0.833333333333333,
                   0.76941935983669, 0.664896503686677, 0.617316856255615,
                   0.429837422497262, 0.82192260740081])

  binranges = numpy.array([0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1])

  actual = histc(x, binranges)

  expected = numpy.array([4, 3, 3, 2, 3, 3, 2, 4, 3, 2, 3, 5, 6, 6, 7, 7, 6, 5, 4, 7])

  numpy.testing.assert_array_equal(actual, expected)







































