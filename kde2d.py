import numpy
import numpy.matlib

import numpy_groupies

from histc import histc

########################################

def K(s):
  return (-1)**s * numpy.prod( numpy.arange(1, 2*s, 2) ) / numpy.sqrt(2*numpy.pi)

########################################

def psi(s, Time):

  global I
  global A2

  # s is a vector
  w = numpy.exp( -I * numpy.pi**2 * Time) *[ 1, .5 * numpy.ones(1, len(I) - 1) ]
  wx = w * ( I**s[0] )
  wy = w * ( I**s[1] )

  return (-1)**sum(s) * ( wy * A2 * wx.conj().T ) * numpy.pi**( 2 * sum(s) )

########################################

def func(s, t):

  global N

  if sum(s) <= 4:
    Sum_func = func( [ s[0] + 1, s[1] ], t ) + func( [ s[0], s[1] + 1 ], t)
    const = ( 1 + 1 / 2**( sum(s) + 1 ) ) / 3
    time = ( -2 * const * K( s[0] ) * K( s[1] ) / N / Sum_func )**( 1 / ( 2 + sum(s) ) )
    out = psi(s, time)
  else:
    out = psi(s, t)

  return out

########################################

def evolve(t):

  global N
  
  Sum_func = func([0,2], t) + func([2,0], t) + 2 * func([1,1], t)
  time = ( 2 * pi * N * Sum_func )**( -1 / 3 )
  out = ( t - time ) / time

  return out, time

########################################

def dct1d(x, weight):

  # Re-order the elements of the columns of x
  nrows = numpy.size(x, 0)
  x = numpy.concatenate( (x[ range(0, nrows, 2), : ], x[ range(nrows-1, 0, -2), : ] ) )

  # Multiply FFT by weights:
  transform1d = numpy.real(weight * numpy.fft.fft(x, axis=0))

  return transform1d

########################################
    
def idct1d(x, weights):

  y = numpy.real( numpy.fft.ifft(weights *x, axis=0) )
 
  nrows, ncols = x.shape
  out = numpy.zeros( (nrows, ncols) )

  mid_nrows = numpy.floor(nrows/2).astype(int) 
  
  out[ range(0, nrows, 2), : ] = y[ range(0, mid_nrows), : ]
  out[ range(1, nrows, 2), : ] = y[ range(nrows-1, mid_nrows-1, -1), : ]

  return out

########################################

def dct2d(data):

  # computes the 2 dimensional discrete cosine transform of data
  # data is an nd cube
  nrows, ncols = numpy.shape(data)
  if nrows != ncols:
    raise ValueError('data is not a square array!')
  
  # Compute weights to multiply DFT coefficients
  left = numpy.array([1])
  right = 2 * ( numpy.exp( -1j * numpy.arange(1,nrows) * numpy.pi / (2*nrows) ) ).T 
  print("shape 1 = {}".format(left.shape) )
  print("shape 2 = {}".format(right.shape) )
  w = numpy.concatenate( [left, right] )
  weight = w[numpy.zeros(ncols,dtype=int)]

  data = dct1d( dct1d(data, weight).conj().T, weight ).conj().T

  return data

########################################

def idct2d(data):

  # computes the 2 dimensional inverse discrete cosine transform
  nrows, ncols = data.shape
  # Compute weights
  w = numpy.exp( 1j * numpy.arange(0,nrows) * numpy.pi / (2*nrows) ).reshape(-1,1)
  weight = w[:, numpy.zeros(ncols,dtype=int)]
  data = idct1d( idct1d(data, weight).conj().T, weight ).T

  return data

########################################

def ndhist(data, M):

  # this function computes the histogram
  # of an n-dimensional data set;
  # 'data' is nrows by n columns
  # M is the number of bins used in each dimension
  # so that 'binned_data' is a hypercube with
  # size length equal to M;

  nrows, ncols = data.shape
  bins = numpy.zeros( (nrows, ncols) )
  for i in range(ncols):
    bins[:,i] = histc( data[:,i], numpy.linspace(0, 1, M+1) )
    bins[:, i] = numpy.minimum( bins[:, i], M )

  # Combine the  vectors of 1D bin counts into a grid of nD bin counts.
  first = bins[numpy.all(bins > 0, 1), :].astype(int) # beware 0's from matlab have not the same meaning as in python
  first[:] = [x-1 for x in first] # arrays start at 0 in python and at 1 in matlab
  second = 1 / nrows
  third = numpy.max(bins, axis =0).astype(int)
  
  binned_data = numpy_groupies.aggregate( first.T, second, size = third )
 
  return binned_data

########################################

def kde2d(data, n = None, MIN_XY = None, MAX_XY = None):

  # fast and accurate state-of-the-art
  # bivariate kernel density estimator
  # with diagonal bandwidth matrix.
  # The kernel is assumed to be Gaussian.
  # The two bandwidth parameters are
  # chosen optimally without ever
  # using/assuming a parametric model for the data or any "rules of thumb".
  # Unlike many other procedures, this one
  # is immune to accuracy failures in the estimation of
  # multimodal densities with widely separated modes (see examples).
  # INPUTS: data - an N by 2 array with continuous data
  #            n - size of the n by n grid over which the density is computed
  #                n has to be a power of 2, otherwise n=2^ceil(log2(n));
  #                the default value is 2^8;
  # MIN_XY,MAX_XY- limits of the bounding box over which the density is computed;
  #                the format is:
  #                MIN_XY=[lower_Xlim,lower_Ylim]
  #                MAX_XY=[upper_Xlim,upper_Ylim].
  #                The dafault limits are computed as:
  #                MAX=max(data,[],1); MIN=min(data,[],1); Range=MAX-MIN;
  #                MAX_XY=MAX+Range/4; MIN_XY=MIN-Range/4;
  # OUTPUT: bandwidth - a row vector with the two optimal
  #                     bandwidths for a bivaroate Gaussian kernel;
  #                     the format is:
  #                     bandwidth=[bandwidth_X, bandwidth_Y];
  #          density  - an n by n matrix containing the density values over the n by n grid;
  #                     density is not computed unless the function is asked for such an output;
  #              X,Y  - the meshgrid over which the variable "density" has been computed;
  #                     the intended usage is as follows:
  #                     surf(X,Y,density)
  # Example (simple Gaussian mixture)
  # clear all
  #   % generate a Gaussian mixture with distant modes
  #   data=[randn(500,2);
  #       randn(500,1)+3.5, randn(500,1);];
  #   % call the routine
  #     [bandwidth,density,X,Y]=kde2d(data);
  #   % plot the data and the density estimate
  #     contour3(X,Y,density,50), hold on
  #     plot(data(:,1),data(:,2),'r.','MarkerSize',5)
  #
  # Example (Gaussian mixture with distant modes):
  #
  # clear all
  #  % generate a Gaussian mixture with distant modes
  #  data=[randn(100,1), randn(100,1)/4;
  #      randn(100,1)+18, randn(100,1);
  #      randn(100,1)+15, randn(100,1)/2-18;];
  #  % call the routine
  #    [bandwidth,density,X,Y]=kde2d(data);
  #  % plot the data and the density estimate
  #  surf(X,Y,density,'LineStyle','none'), view([0,60])
  #  colormap hot, hold on, alpha(.8)
  #  set(gca, 'color', 'blue');
  #  plot(data(:,1),data(:,2),'w.','MarkerSize',5)
  #
  # Example (Sinusoidal density):
  #
  # clear all
  #   X=rand(1000,1); Y=sin(X*10*pi)+randn(size(X))/3; data=[X,Y];
  #  % apply routine
  #  [bandwidth,density,X,Y]=kde2d(data);
  #  % plot the data and the density estimate
  #  surf(X,Y,density,'LineStyle','none'), view([0,70])
  #  colormap hot, hold on, alpha(.8)
  #  set(gca, 'color', 'blue');
  #  plot(data(:,1),data(:,2),'w.','MarkerSize',5)
  #
  # Notes: If you have a more accurate density estimator 
  #        (as measured by which routine attains the smallest 
  #         L_2 distance between the estimate and the true density) or you have 
  #        problems running this code, please email me at botev@maths.uq.edu.au 


  #  Reference: Z. I. Botev, J. F. Grotowski and D. P. Kroese
  #             "KERNEL DENSITY ESTIMATION VIA DIFFUSION" ,Submitted to the
  #             Annals of Statistics, 2009

  global N 
  global A2 
  global I
  
  if n is None:
    n = 2**8
  
  n = 2**numpy.ceil(numpy.log2(n)) # round up n to the next power of 2;
  N = numpy.size(data, 0)
  
  if MIN_XY is None:
    MAX = max(data, [], 1) 
    MIN = min(data, [], 1)
    Range = MAX - MIN
    MAX_XY = MAX + Range / 4 
    MIN_XY = MIN - Range / 4
  
  scaling = MAX_XY - MIN_XY
  print("size = {}".format(numpy.size(data,1)))
  if N <= numpy.size(data, 1):
    raise ValueError('data has to be an N by 2 array where each row represents a two dimensional observation')
  
  transformed_data = ( data - numpy.matlib.repmat(MIN_XY,N,1)) / numpy.matlib.repmat(scaling,N,1)
  #bin the data uniformly using regular grid
  initial_data = ndhist(transformed_data,n)
  # discrete cosine transform of initial data
  a= dct2d(initial_data);
  # now compute the optimal bandwidth^2
  I = range(n)**2
  A2 = a**2

  t_star = fzero(lambda t: t-evolve(t), [0,0.1])

  p_02 = func([0,2], t_star)
  p_20 = func([2,0], t_star)
  p_11 = func([1,1], t_star)
  t_y = (p_02^(3/4)/(4*pi*N*p_20^(3/4) * (p_11+sqrt(p_20*p_02))))**(1/3)
  t_x = (p_20^(3/4)/(4*pi*N*p_02^(3/4) * (p_11+sqrt(p_20*p_02))))**(1/3)
  # smooth the discrete cosine transform of initial data using t_star
  a_t = numpy.exp( -range(n).T**2 * pi**2 * t_x / 2 ) * numpy.exp(-range(n)**2 * pi**2 * t_y / 2 ) * a 
  # now apply the inverse discrete cosine transform
  if nargout > 1:
    density = idct2d(a_t) * (numel(a_t) / prod(scaling))
    [X, Y] = meshgrid( numpy.arange( MIN_XY[0], MAX_XY[0], scaling[0] / (n-1) ), numpy.arange( MIN_XY[1], MAX_XY[1], scaling[1] / (n-1) ) )
  
  bandwidth = sqrt( [t_x, t_y] ) * scaling 

  return bandwidth, density, X, Y 

